/**
 * Stanley W. Gleason
 */

angular.module('app', ['ui.router', 'app.controllers', 'app.services'])

.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider

	.state('app', {
		url: "/app",
		abstract: true,
		template: '<ui-view/>'
	})

	.state('app.home', {
        url: '/home',
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
    });

	$urlRouterProvider.otherwise('/app/home');
});