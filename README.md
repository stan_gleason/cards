# Deck of Cards by Stanley W. Gleason
> Sort and shuffle a deck of cards

## Step 1: Clone the project
```shell
$ git clone git@bitbucket.org:stan_gleason/cards.git
$ cd cards
$ npm install
```

## Step 2: Run tests
```shell
$ karma start karma.conf.js
```

## Step 3: Launch app
- Place folder in http server and navigate to it within browser

## Note:
The sort and shuffle functions are located in `core/DeckOfCards.js`