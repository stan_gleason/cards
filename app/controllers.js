/**
 * Stanley W. Gleason
 */

angular.module('app.controllers', [])

.controller('HomeCtrl', function($scope, DeckFactory) {
	var columns;

	var _init = function _init() {
		columns = 4;

		$scope.title = "Deck of Cards";
		$scope.cards = listToMatrix(DeckFactory.sort(), columns);
	};

	var listToMatrix = function listToMatrix(list, elementsPerSubArray) {
		var matrix = [], i, k;
		for (i = 0, k = -1; i < list.length; i++) {
			if (i % elementsPerSubArray === 0) {
				k++;
				matrix[k] = [];
			}
			matrix[k].push(list[i]);
		}
		return matrix;
	};

	$scope.sort = function() {
		$scope.cards = listToMatrix(DeckFactory.sort(), columns);
	};

	$scope.shuffle = function() {
		$scope.cards = listToMatrix(DeckFactory.shuffle(), columns);
	};

	_init();
});