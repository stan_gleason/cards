/**
 * Stanley W. Gleason
 */

var Deck = (function() {
	
	var _deck = [];

	var _init = function _init() {
		// Create deck of 52 cards
		for (var rankKey in Rank) {
			if (Rank.hasOwnProperty(rankKey)) {
				var rank = Rank[rankKey];
				for (var suitKey in Suit) {
					if(Suit.hasOwnProperty(suitKey)) {
						var suit = Suit[suitKey];
						_deck.push(new Card(rank, suit));
					}
				}
			}
		}
	};

	var _sort = function _sort() {

		_deck.sort(function(a, b) {
			if(a.rank() < b.rank()) {
				return -1;
			}
			if(a.rank() > b.rank()) {
				return 1;
			}

			if(a.suit() < b.suit()) {
				return -1;
			} 
			if(a.suit() > b.suit()) {
				return 1;
			}

			return 0;
		});

		return _deck;
	};

	var _shuffle = function _shuffle() {
		for(i = 0; i < _deck.length; i++) {
			var randomIndex = Math.floor((Math.random() * _deck.length));
			var tmp = _deck[i];
			_deck[i] = _deck[randomIndex];
			_deck[randomIndex] = tmp;
		}
		return _deck;
	};

	var _rankChar = function _rankChar(card) {
		var character = "";
		switch(card.rank()) {
			case Rank.ACE:
				character = "A";
				break;
			case Rank.TWO:
				character = "2";
				break;
			case Rank.THREE:
				character = "3";
				break;
			case Rank.FOUR:
				character = "4";
				break;
			case Rank.FIVE:
				character = "5";
				break;
			case Rank.SIX:
				character = "6";
				break;
			case Rank.SEVEN:
				character = "7";
				break;
			case Rank.EIGHT:
				character = "8";
				break;
			case Rank.NINE:
				character = "9";
				break;
			case Rank.TEN:
				character = "0";
				break;
			case Rank.JACK:
				character = "J";
				break;
			case Rank.QUEEN:
				character = "Q";
				break;
			case Rank.KING:
				character = "K";
				break;
		}
		return character;
	};

	var _suitChar = function _suitChar(card) {
		var character = "";
		switch(card.suit()) {
			case Suit.SPADE:
				character = "S";
				break;
			case Suit.HEART:
				character = "H";
				break;
			case Suit.CLUB:
				character = "C";
				break;
			case Suit.DIAMOND:
				character = "D";
				break;
		}
		return character;
	};

	var _imgUrl = function _imgUrl(card) {
		return "http://deckofcardsapi.com/static/img/" + _rankChar(card) + _suitChar(card) + ".png"
	};

	var Suit = {
		SPADE: 1,
		HEART: 2,
		CLUB: 3,
		DIAMOND: 4
	};

	var Rank = {
		ACE: 1,
		TWO: 2,
		THREE: 3,
		FOUR: 4,
		FIVE: 5,
		SIX: 6,
		SEVEN: 7,
		EIGHT: 8,
		NINE: 9,
		TEN: 10,
		JACK: 11,
		QUEEN: 12,
		KING: 13
	};

	function Card (rank, suit) {
	    this._rank = rank;
	    this._suit = suit;
	    this._photo = _imgUrl(this);
	}

	Card.prototype.constructor = Card;

	Card.prototype.rank = function() {
		return this._rank;
	};

	Card.prototype.suit = function() {
		return this._suit;
	};

	Card.prototype.photo = function() {
		return this._photo;
	};

	_init();

	return {
		sort: _sort,
		shuffle: _shuffle,
		Rank: Rank,
		Suit: Suit
	};

})();