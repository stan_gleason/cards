/**
 * Stanley W. Gleason
 */

angular.module('app.services', [])

.factory('DeckFactory', function() {
	var DeckService = {};
	DeckService.sort = function() {
		return Deck.sort();
	};
	DeckService.shuffle = function() {
		return Deck.shuffle();
	};
	return DeckService;
});