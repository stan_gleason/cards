describe("Deck", function() {
	describe("Deck.sort()", function() {

		var sorted
			, cards;
		
		beforeEach(function() {
			sorted = Deck.sort();

			cards = [];
			for (var rankKey in Deck.Rank) {
				if (Deck.Rank.hasOwnProperty(rankKey)) {
					var rank = Deck.Rank[rankKey];
					for (var suitKey in Deck.Suit) {
						if(Deck.Suit.hasOwnProperty(suitKey)) {
							var suit = Deck.Suit[suitKey];
							cards.push({
								suit: suit,
								rank: rank
							});
						}
					}
				}
			}
		});

		it("should return an array of length 52", function() {
			expect(sorted.length).toEqual(52);
		});

		it("should sort an array of cards", function() {
			var equals = true;
			for(i = 0; i < cards.length; i++) {
				if(sorted[i].suit() !== cards[i].suit ||
					sorted[i].rank() !== cards[i].rank) {
					equals = false;
					break;
				}
			}
			expect(equals).toBeTruthy();
		});
	});

	describe("Deck.shuffle()", function() {

		var sorted
			, shuffled;
		
		beforeEach(function() {

			function populateSorted(deck) {
				for(i = 0; i < deck.length; i++) {
					sorted.push(deck[i])
				}
			}

			sorted = [];
			populateSorted(Deck.sort());
			shuffled = Deck.shuffle();

		});

		it("should return an array of length 52", function() {
			expect(shuffled.length).toEqual(52);
		});

		it("should not equal a sorted deck of cards", function() {
			var equals = true;
			for(i = 0; i < shuffled.length; i++) {
				if(sorted[i].suit() !== shuffled[i].suit()) {
					equals = false;
					break;
				}
			}
			expect(equals).toBeFalsy();
		});
	});
});